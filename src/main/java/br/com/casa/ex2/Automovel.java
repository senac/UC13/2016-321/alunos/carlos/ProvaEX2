
package br.com.casa.ex2;


public class Automovel {
    
    private double custoDeFabrica;
    private double custoConseumidor;
    private double percentualDistribuidor;
    private double imposto;

    public Automovel() {
    }

    public Automovel(double custoDeFabrica, double percentualDistribuidor, double imposto) {
        this.custoDeFabrica = custoDeFabrica;
        this.percentualDistribuidor = percentualDistribuidor;
        this.imposto = imposto;
    }
    

    public double getCustoDeFabrica() {
        return custoDeFabrica;
    }

    public void setCustoDeFabrica(double custoDeFabrica) {
        this.custoDeFabrica = custoDeFabrica;
    }

    public double getCustoConseumidor() {
        return custoConseumidor;
    }

    public void setCustoConseumidor(double custoConseumidor) {
        this.custoConseumidor = custoConseumidor;
    }

    public double getPercentualDistribuidor() {
        return percentualDistribuidor;
    }

    public void setPercentualDistribuidor(double percentualDistribuidor) {
        this.percentualDistribuidor = percentualDistribuidor;
    }

    public double getImposto() {
        return imposto;
    }

    public void setImposto(double imposto) {
        this.imposto = imposto;
    }
    
    
    
    
    
}
