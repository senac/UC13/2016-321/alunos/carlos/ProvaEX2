
package br.com.casa.ex2.test;

import br.com.casa.ex2.Automovel;
import br.com.casa.ex2.Calculadora;
import org.junit.Test;
import static org.junit.Assert.*;

public class CalculadoraTest {
    
    @Test
    public void deveCalcularOcustoDoCarroAoConsumidor(){
        Calculadora calculadora = new Calculadora();
        Automovel a = new Automovel(15.000, 0.28, 0.45);
        
        double resultado = calculadora.calcular(a);
        assertEquals(25.950, resultado, 0.001);
        
        
        
        
        
    }
    
}
